#include <hello_world.h>

namespace UniMacedonia {

std::string_view hello_world() {
    return "hello world";
}

} // end namespace