#pragma once

#include <string_view>
#include <hello_lib_export.h>

namespace UniMacedonia {
    HELLO_LIB_EXPORT std::string_view hello_world();
}
